package com.flavrei.projetappmobile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView villeRecyclerView;
    private List<Ville> listeVilles;
    private VilleAdapter villeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        villeRecyclerView = (RecyclerView)findViewById(R.id.myRecyclerView);
        listeVilles = new ArrayList<>();

        listeVilles.add(new Ville("Paris", 20));
        listeVilles.add(new Ville("Los Angeles", 32));
        listeVilles.add(new Ville("Toronto", -10));

        villeAdapter = new VilleAdapter(listeVilles);
        villeRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        SnapToBlock snapToBlock = new SnapToBlock(1);
        snapToBlock.attachToRecyclerView(villeRecyclerView);
        villeRecyclerView.setAdapter(villeAdapter);
    }
}