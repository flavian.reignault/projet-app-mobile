package com.flavrei.projetappmobile;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class VilleAdapter extends RecyclerView.Adapter<VilleAdapter.VilleViewHolder> {

    List<Ville> listeVilles;

    VilleAdapter(List<Ville> listeVilles) {
        this.listeVilles = listeVilles;
    }

    @Override
    public VilleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.ville_item, parent, false);
        return new VilleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VilleViewHolder holder, int position) {
        holder.diplay(listeVilles.get(position));
    }

    @Override
    public int getItemCount() {
        return listeVilles.size();
    }

    class VilleViewHolder extends RecyclerView.ViewHolder {

        private TextView villeTV;
        private TextView temperatureTV;

        VilleViewHolder(View itemView) {
            super(itemView);
            villeTV = (TextView)itemView.findViewById(R.id.ville);
            temperatureTV = (TextView)itemView.findViewById(R.id.temperature);
        }

        void diplay(Ville jeuVideo) {
            villeTV.setText(jeuVideo.getVille());
            temperatureTV.setText(jeuVideo.getTemperature() + "°C");
        }
    }
}
