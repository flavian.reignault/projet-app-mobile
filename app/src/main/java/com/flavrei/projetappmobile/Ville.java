package com.flavrei.projetappmobile;

public class Ville {

    private String ville;
    private int temperature;

    Ville(String ville, int temperature) {
        this.ville = ville;
        this.temperature = temperature;
    }

    public String getVille() {return this.ville;}
    public int getTemperature() {return this.temperature;}
}
